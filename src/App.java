import java.text.Collator;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println(precio());
        //proof(10);
        //System.out.println(restador());
        //System.out.println(pitagoras());
        //mayusculador();
        //System.out.println("A" > "a");
        mayusculador();
    }

    public static String precio(){
        Scanner read1 = new Scanner(System.in);
        System.out.print("Precio de venta por unidad (Sin IVA): ");
        var price = read1.nextInt();
        
        
        Scanner read2 = new Scanner(System.in);
        System.out.print("Número de artículos: ");
        var number = read2.nextInt();
        

        Scanner read3 = new Scanner(System.in);
        System.out.print("Porcentaje de IVA: ");
        var IVA = read3.nextFloat(); 
        

        var total = number*price*IVA;

        read1.close();
        read2.close();
        read3.close();

        return "El valor total es " + total;
    }

    public static void proof(int n){
        /*Scanner sc = new Scanner(System.in);
        System.out.print("Inserte un número: ");
        var n = sc.nextInt();
        */
        var result = (n%2 == 0) ? n*2 : n*3;

        System.out.println(result);
        
    }

    public static String restador(){
        Scanner sc = new Scanner(System.in);
        System.out.print("Escriba el número M: ");
        var M = sc.nextInt();

        Scanner sc2 = new Scanner(System.in);
        System.out.print("Escriba el número m: ");
        var m = sc2.nextInt();

        var r = (m == 1) ? m*10 : Math.pow(10,m); 



        var result = Math.floor(M/r);

        sc.close();
        sc2.close();

        return "Al restarle " + m + " cifras al número " + M + " se obtiene: " + result;

        
    }


    public static String pitagoras(){

        Scanner sc1 = new Scanner(System.in);
        System.out.print("Inserte el cateto menor del triángulo: ");
        var cm = sc1.nextInt();

        Scanner sc2 = new Scanner(System.in);
        System.out.print("Inserte el cateto menor del triángulo: ");
        var cM = sc2.nextInt();

        var H = (cm > 0 && cM >0) ? Math.sqrt(Math.pow(cm,2)+Math.pow(cM,2)) : "No es posible insertar valores negativos";

        return "" + H;

    }

        public static void mayusculador(){

            Scanner sc = new Scanner(System.in);
            System.out.print("Inserte una letra: ");
            String letter = sc.next();

            var letra = letter.toLowerCase();

            Collator comparador = Collator.getInstance();
            comparador.setStrength(Collator.TERTIARY);
            System.out.println();

            var r = (comparador.compare(letter, letra) == 1 ) ? "La letra es mayúscula" : " La letra es minúscula";


            System.out.println(r);

        


        

        }

    


}
